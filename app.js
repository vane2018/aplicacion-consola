require('colors')
const {inquirerMenu,
    pausa,
   leerInput,
   listadoTareasBorrar,
confirmar,
mostrarListadoChecklist } = require('./helpers/inquirer');

const Tareas = require('./models/tareas')
const Tarea = require('./models/tarea');
const { guardarDB ,leerDB} = require('./helpers/guardarArchivo');

console.clear();
const main = async () => {
   
    let opt='';
    let ver='';
    const tareas= new Tareas();
    const tareasDB = leerDB();

    if (tareasDB) {
       tareas.cargarTareasFromArray(tareasDB);
      }
  

    do{

        
       opt= await inquirerMenu();
       switch (opt) {
          case '1':
             const desc = await leerInput('descripcion:')
           
             tareas.crearTarea(desc);
             break;

      case '2':
             console.log("Ana ingreso");
         
                console.log(tareas.listadoCompleto());
             
               break;

      case '3':
                
               tareas.listadoCompletoPendiente(true);
               
                  
                  break;
         
      case '4':
                                    
              tareas.listadoCompletoPendiente(false);
                  break;

     case '5':
        //completado o pendiente
                              const ids= await mostrarListadoChecklist(tareas.listadoArr)      
                    console.log(ids);

                    tareas.toggleCompletadas(ids);
                         break;

      case '6':
               
                   const id =  await listadoTareasBorrar(tareas.listadoArr);
if (id !=='0') {
   const ok= await confirmar('esta seguro de borrar?')
   
   if(ok){
      tareas.borrarTarea(id);
      console.log('tarea borrada');
   }
   
}
                    break;
       
          default:
             break;
       }

      guardarDB(tareas.listadoArr);
       await pausa();

      
     //guardarDB(tareas.listadoArr)
    /*    console.log({opt});
       if (opt!=='0') await pausa(); */
    }while (opt !== '0');
   /*  mostrarMenu();
    pausa(); */
}
main();